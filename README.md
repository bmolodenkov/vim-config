# Vim based IDE setup

## Install
1. [vim-plug](https://github.com/junegunn/vim-plug)
2. [coc.nvim](https://github.com/neoclide/coc.nvim)
3. [ccls](https://github.com/MaskRay/ccls)

