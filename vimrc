call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'jackguo380/vim-lsp-cxx-highlight'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'ctrlpvim/ctrlp.vim'

Plug 'ErichDonGubler/vim-sublime-monokai'
Plug 'ryanoasis/vim-devicons'

Plug 'preservim/nerdtree'
Plug 'justinmk/vim-sneak'

Plug 'editorconfig/editorconfig-vim'
Plug 'rhysd/vim-clang-format'

Plug 'pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build', 'branch': 'main' }
Plug 'rust-lang/rust.vim'

Plug 'vlime/vlime', {'rtp': 'vim/'}


" Initialize plugin system
call plug#end()

inoremap jk <ESC>

runtime macros/matchit.vim
syntax on

set number
set noswapfile
set hlsearch
set ignorecase
set incsearch

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

filetype plugin indent on

"set mouse=a

let g:airline_powerline_fonts = 1 "Включить поддержку Powerline шрифтов
let g:airline#extensions#keymap#enabled = 0 "Не показывать текущий маппинг
let g:airline_section_z = "\ue0a1:%l/%L Col:%c" "Кастомная графа положения курсора
let g:Powerline_symbols='unicode' "Поддержка unicode
let g:airline#extensions#xkblayout#enabled = 0 "Про это позже расскажу

set so=10

colorscheme sublimemonokai
"colorscheme ron

" workaround WSL background issue
set t_ut=

let &t_SI.="\e[5 q" "SI = режим вставки
let &t_SR.="\e[3 q" "SR = режим замены
let &t_EI.="\e[1 q" "EI = нормальный режим
"Где 1 - это мигающий прямоугольник
""2 - обычный прямоугольник
""3 - мигающее подчёркивание
""4 - просто подчёркивание
""5 - мигающая вертикальная черта
""6 - просто вертикальная черта

source ~/repos/vim-config/default-coc.vim

" disable defined in default coc
autocmd! CursorHold *

autocmd FileType c,cpp,h ClangFormatAutoEnable

set grepprg=ag\ --nogroup\ --no-color

command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
" Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
let g:ctrlp_user_command = 'ag %s -l -U --nocolor -g ""'
let g:ctrlp_use_caching = 1

highlight CocFloating ctermbg=Black
let g:rustfmt_autosave = 1
let g:lisp_rainbow = 1

let g:vlime_window_settings = {
        \ "repl": {
            \ "pos": "botright",
            \ "vertical": v:true
        \ }
    \ }

fu! MyTabLabel(n)
  let buflist = tabpagebuflist(a:n)
  let winnr = tabpagewinnr(a:n)
  let string = fnamemodify(bufname(buflist[winnr - 1]), ':t')
  return empty(string) ? '[unnamed]' : string
endfu

fu! MyTabLine()
  let s = ''
  for i in range(tabpagenr('$'))
  " select the highlighting
      if i + 1 == tabpagenr()
      let s .= '%#TabLineSel#'
      else
      let s .= '%#TabLine#'
      endif

      " set the tab page number (for mouse clicks)
      "let s .= '%' . (i + 1) . 'T'
      " display tabnumber (for use with <count>gt, etc)
      let s .= ' '. (i+1) . ' ' 

      " the label is made by MyTabLabel()
      let s .= ' %{MyTabLabel(' . (i + 1) . ')} '

      if i+1 < tabpagenr('$')
          let s .= ' |'
      endif
  endfor
  return s
endfu
set tabline=%!MyTabLine()
